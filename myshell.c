/*
 * This code implements a simple shell program
 * It supports the internal shell command "exit", 
 * backgrounding processes with "&", input redirection
 * with "<" and output redirection with ">".
 * However, this is not complete.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>

extern char **getaline();

//added prototypes
void pipeItUp(char **args);
int ampersand(char **args);
void do_command(char **args, int block, int isPiped,
	int input, char *input_filename,
	int output, char *out_filename);
int internal_command(char **args);
int redirect_input(char **args, char **input_filename);
int redirect_output(char **args, char **output_filename);
int helpipe(int in, int out, char** args);
/*
 * Handle exit signals from child processes
 */
void sig_handler(int signal) {
  int status;
  int result = wait(&status);

  printf("Wait returned %d\n", result);
}

/*
 * The main shell function
 */ 
int main() {
  int i;
  char **args; 
  int result;
  int block;
  int output;
  int input;
  int piped;
  char *output_filename;
  char *input_filename;

  // Set up the signal handler
  sigset(SIGCHLD, sig_handler);

  // Loop forever
  while(1) {

    // Print out the prompt and get the input
    printf("->");
    args = getaline();

    // No input, continue
    if(args[0] == NULL)
      continue;
  
    // Check for internal shell commands, such as exit
    if(internal_command(args))
      continue;

    // Check for an ampersand
    block = (ampersand(args) == 0);
	
	// Check for bar
	piped = bar(args);

    // Check for redirected input
    input = redirect_input(args, &input_filename);

    switch(input) {
    case -1:
      printf("Syntax error!\n");
      continue;
      break;
    case 0:
      break;
    case 1:
      printf("Redirecting input from: %s\n", input_filename);
      break;
    }

    // Check for redirected output
    output = redirect_output(args, &output_filename);

    switch(output) {
    case -1:
      printf("Syntax error!\n");
      continue;
      break;
    case 0:
      break;
    case 1:
      printf("Redirecting output to: %s\n", output_filename);
      break;
    case 2:
      printf("Appending output to: %s\n", output_filename);
      break;
    }

    // Do the command
    do_command(args, block, piped,
	       input, input_filename, 
	       output, output_filename);
	}
}


/*
 * Check for ampersand as the last argument
 */
int ampersand(char **args) {
  int i;

  for(i = 1; args[i] != NULL; i++) ;

  if(args[i-1][0] == '&') {
    free(args[i-1]);
    args[i-1] = NULL;
    return 1;
  } else {
    return 0;
  }
  
  return 0;
}

/* 
 * Check for internal commands
 * Returns true if there is more to do, false otherwise 
 */
int internal_command(char **args) {
  if(strcmp(args[0], "exit") == 0) {
    exit(0);
  }

  return 0;
}

/*
 * Check for bars, return 1 if found
 */
int bar(char **args){
	int i;

	for(i = 0; args[i] != NULL; i++){

		if(args[i][0] == '|') {			
			return 1;
		}
	}
	return 0;
}

/*
 * Detect, count, and free pipe shit
 */
void pipeItUp(char **args) {
	//File descriptors for pipe IO
	int fd[2];
	int fd2[2];
	//number of commands
	int nCmds = 0;
	char *commands[256];
	pid_t pid;
	// variables for different loops that go through args
	int i = 0;
	int j = 0;
	int k = 0;
	int l = 0;
	//variable for escaping loop when reaching end of args
	int pipend = 0;
	
	// count number of commands - anything that isn't a |
	while (args[l] != NULL){
		if (strcmp(args[l],"|") == 0){
			nCmds++;
		}
		l++;
	}
	nCmds++;
	
	// Main loop where we copy everything from args to our new set of commands until we reach |
	while (args[j] != NULL && end != 1){
		k = 0;
		while (strcmp(args[j],"|") != 0){
			commands[k] = args[j];
			j++;	
			//if we've reached the end of args, exit loop
			if (args[j] == NULL){
				end = 1;
				k++;
				break;
			}
			//start copying next command
			k++;
		}
		// null the pipe character
		commands[k] = NULL;
		j++;		
		//if odd, pipe to an input FD, if even, pipe to output FD
		if (i % 2 != 0){
			pipe(fd); 
		}else{
			pipe(fd2); 
		}
		
		//start child process/next command
		pid=fork();
		//check for error in forking
		if(pid==-1){			
			if (i != nCmds - 1){
				if (i % 2 != 0){
					close(fd[1]); 
				}else{
					close(fd2[1]); 
				} 
			}			
			printf("Child process creation was unsuccessful\n\r");
			return;
		}
		// child's first command
		if(pid==0){
			if (i == 0){
				dup2(fd2[1], STDOUT_FILENO);
			}
			// In the last command, based on pipe's even or oddness, 
			//preserve stdout for display in shell or redirection to a file
			else if (i == nCmds - 1){
				if (nCmds % 2 != 0){
					dup2(fd[0],STDIN_FILENO);
				}else{
					dup2(fd2[0],STDIN_FILENO);
				}
			// In middle commands, create pipes for both process's input and output
			}else{ 
				if (i % 2 != 0){
					dup2(fd2[0],STDIN_FILENO); 
					dup2(fd[1],STDOUT_FILENO);
				}else{ 
					dup2(fd[0],STDIN_FILENO); 
					dup2(fd2[1],STDOUT_FILENO);					
				} 
			}
			
			//terminate erroneous commands
			if (execvp(commands[0],commands) == -1){
				kill(getpid(),SIGTERM);
			}		
		}
				
		//close ends of both pipes based on even and oddness
		if (i == 0){
			close(fd2[1]);
		}
		else if (i == nCmds - 1){
			if (nCmds % 2 != 0){					
				close(fd[0]);
			}else{					
				close(fd2[0]);
			}
		}else{
			if (i % 2 != 0){					
				close(fd2[0]);
				close(fd[1]);
			}else{					
				close(fd[0]);
				close(fd2[1]);
			}
		}
			//wait on the process to finish	
		waitpid(pid,NULL,0);
				
		i++;	
	}
}



/* 
 * Do the command
 */
//changed from int do_command to void do_command
void do_command(char **args, int block, int piped,
	       int input, char *input_filename,
	       int output, char *output_filename) {
  
  int result;
  pid_t child_id;
  int status;
  

  // Fork the child process
  child_id = fork();

  // Check for errors in fork()
  switch(child_id) {
  case EAGAIN:
    perror("Error EAGAIN: ");
    return;
  case ENOMEM:
    perror("Error ENOMEM: ");
    return;
  }

  if(child_id == 0) {

    // Set up redirection in the child process
    if(input)
      freopen(input_filename, "r", stdin);
    //case 1 is '>'
    if(output == 1)
      freopen(output_filename, "w+", stdout);
    //case 2 is '>>'
    if(output == 2)
      freopen(output_filename, "a+", stdout);
	
	if(piped) {
		pipeItUp(args);
		exit(-1);
	}
    // Execute the command
    result = execvp(args[0], args);

    exit(-1);
  }

  // Wait for the child process to complete, if necessary
  if(block) {
    printf("Waiting for child, pid = %d\n", child_id);
    result = waitpid(child_id, &status, 0);
  }
}

/*
 * Check for input redirection
 */
int redirect_input(char **args, char **input_filename) {
	int i;
	int j;

	for(i = 0; args[i] != NULL; i++) {

		// Look for the <
		if(args[i][0] == '<') {
			free(args[i]);

			// Read the filename
			if(args[i+1] != NULL) {
				*input_filename = args[i+1];
			}else {
				return -1;
			}

			// Adjust the rest of the arguments in the array
			for(j = i; args[j-1] != NULL; j++) {
				args[j] = args[j+2];
			}
			return 1;
		}
	}
	return 0;
}


/*
 * Check for output redirection
 */
int redirect_output(char **args, char **output_filename) {
	int i;
	int j;

	for(i = 0; args[i] != NULL; i++) {
	 
		//Look for >> before you look for a single >
		if(args[i][0] == '>'){

			if(args[i+1][0] == '>') {
				//printf(" this is working ");

				free(args[i]);
				free(args[i+1]);
				
				if(args[i+2] != NULL) {
					*output_filename = args[i+2];
					//printf(" filename %C\n", **output_filename);
				}else{
					return -1;
				}

				for(j = i; args[j-1] != NULL; j++) {
					args[j] = args[j+3];
				}
				return 2;
			}else {
				free(args[i]);
				//get the filename
				if(args[i+1] != NULL) {
					*output_filename = args[i+1];
					//printf(" filename is %c\n", **output_filename);
				}else {
					return -1;
				}
				//adjust the rest of the arguments in the array
				for(j = i; args[j-1] != NULL; j++) {
					args[j] = args[j+2];
				}
				return 1;
			}
		}
	}
	return 0;
}


